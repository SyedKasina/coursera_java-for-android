package mooc.vandy.java4android.calculator.logic;

/**
 * Define common math. operation
 */
public interface MathOperation {

    /**
     * Calculation of two numbers @valA and @valB.
     *
     * @param valA
     * @param valB
     * @return String which can be printed.
     */
    String calc(int valA, int valB);
}

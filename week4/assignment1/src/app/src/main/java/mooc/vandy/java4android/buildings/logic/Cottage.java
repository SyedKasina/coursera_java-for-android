package mooc.vandy.java4android.buildings.logic;

/**
 * This is the cottage class file.  It is a subclass of House.
 */
public class Cottage extends House {

    private  boolean mSecondFlor;

    public Cottage(final int dimension, final int lotLength, final int lotWidth) {
        super(dimension, dimension, lotLength, lotWidth);
    }

    public Cottage(final int dimension, final int lotLength, final int lotWidth, String owner, boolean secondFloor) {
        super(dimension, dimension, lotLength, lotWidth, owner);
        this.mSecondFlor = secondFloor;
    }

    public boolean hasSecondFloor() {
        return this.mSecondFlor;
    }

    @Override
    public String toString() {
        return mSecondFlor ? "is a two story cottage;" : "";
    }
}

